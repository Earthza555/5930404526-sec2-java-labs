package wiriyasrisuwatthana.surawit.lab9;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import wiriyasrisuwatthana.surawit.lab8.PatientFormV6;
/**
 * This program is about add slider to patientformv6 and show dialog
 * 
 * @author Surawit Wiriyasrisuwatthana
 * 
 * @version 1.0
 * 
 */
public class PatientFormV7 extends PatientFormV6 implements ChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6535629828180097825L;
	protected JPanel sliderPanel, patientFormV7Panel, botPanel;
	protected JLabel topNumLabel, bottomNumLabel;
	protected JSlider topSlider, bottomSlider;

	public PatientFormV7(String string) {
		super(string);
	}

	// main method
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	// listener
	protected void addListeners() {
		super.addListeners();
		topSlider.addChangeListener(this);
		bottomSlider.addChangeListener(this);
	}

	// createAndShowGUI Method
	public static void createAndShowGUI() {
		PatientFormV7 patientForm7 = new PatientFormV7("Patient Form V7");
		patientForm7.addComponents();
		patientForm7.setFrameFeatures();
		patientForm7.addListeners();
	}

	// addComponents method
	public void addComponents() {
		pack();
		super.addComponents();
		patientFormV7Panel = new JPanel(new BorderLayout());
		botPanel = new JPanel(new BorderLayout());

		topNumLabel = new JLabel("Top number");
		bottomNumLabel = new JLabel("Bottom number");
		sliderPanel = new JPanel(new GridLayout());
		sliderPanel.setLayout(new GridLayout(0, 2));
		sliderPanel.setBorder(BorderFactory.createTitledBorder("Blood Pressure"));

		topSlider = new JSlider(0, 200, 100);
		topSlider.setMajorTickSpacing(50);
		topSlider.setMinorTickSpacing(10);
		topSlider.setPaintTicks(true);
		topSlider.setPaintLabels(true);
		topSlider.setName("Top number of Blood Pressure ");

		bottomSlider = new JSlider(0, 200, 100);
		bottomSlider.setMajorTickSpacing(50);
		bottomSlider.setMinorTickSpacing(10);
		bottomSlider.setPaintTicks(true);
		bottomSlider.setPaintLabels(true);
		bottomSlider.setName("Bottom number of Blood Pressure ");

		sliderPanel.add(topNumLabel);
		sliderPanel.add(topSlider);
		sliderPanel.add(bottomNumLabel);
		sliderPanel.add(bottomSlider);

		botPanel.add(addressPanel, BorderLayout.NORTH);
		botPanel.add(typePanel, BorderLayout.CENTER);
		botPanel.add(mswPanel, BorderLayout.SOUTH);

		patientFormV7Panel.add(patientFormV6Panel, BorderLayout.NORTH);
		patientFormV7Panel.add(sliderPanel, BorderLayout.CENTER);
		patientFormV7Panel.add(botPanel, BorderLayout.SOUTH);
		add(patientFormV7Panel);
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		JSlider source = (JSlider) e.getSource();
		if (!source.getValueIsAdjusting()) {
			int value = source.getValue();
			JOptionPane.showMessageDialog(this, source.getName() + "is " + value);
		}
	}

	//handleOKButton Method
	public void handleOKButton() {

		String showingMessage = "Name = " + nameTextField.getText() + " Birthdate = " + birthDateTextField.getText()
				+ " Weight = " + weightTextField.getText() + " Height = " + heightTextField.getText() + "\n"
				+ "Gender = " + (maleButton.isSelected() ? "Male" : (femaleButton.isSelected() ? "Female" : "")) + "\n"
				+ "Address = " + addressText.getText() + "\n" + "Type = " + typeComboBox.getSelectedItem()
				+ "\nBlood Pressure : Top Number = " + topSlider.getValue() + ",Bottom Number = "
				+ bottomSlider.getValue();

		JOptionPane.showMessageDialog(null, showingMessage, "Message", JOptionPane.PLAIN_MESSAGE);

	}

}
