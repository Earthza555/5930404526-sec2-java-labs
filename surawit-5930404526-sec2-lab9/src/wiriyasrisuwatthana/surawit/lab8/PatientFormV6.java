package wiriyasrisuwatthana.surawit.lab8;

/*	
 * This is program as same as PatientFormV5 but add image
 * 
 *	@author  Surawit Wiriyasrisuwatthana 
 * 	
 * 	@version 1.0
 * 
 *  23/03/2017
 * 
 */
import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.*;
import java.net.URL;
import javax.imageio.ImageIO;
import javax.swing.*;

public class PatientFormV6 extends PatientFormV5 {
	private static final long serialVersionUID = 1L;

	// create image icon
	ImageIcon iconOpen = new ImageIcon("bin/wiriyasrisuwatthana/surawit/lab8/openIcon.png");
	ImageIcon iconSave = new ImageIcon("bin/wiriyasrisuwatthana/surawit/lab8/saveIcon.png");
	ImageIcon iconExit = new ImageIcon("bin/wiriyasrisuwatthana/surawit/lab8/quitIcon.png");
	protected JPanel picPanel, patientFormV6Panel;

	public PatientFormV6(String string) {
		super(string);
	}

	// main method
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	// createAndShowGUI Method
	public static void createAndShowGUI() {
		PatientFormV6 patientForm6 = new PatientFormV6("Patient Form V6");
		patientForm6.addComponents();
		patientForm6.setFrameFeatures();
		patientForm6.addListeners();
	}

	public ImageIcon createImageIcon(String path, String description) {
		java.net.URL imgURL = getClass().getResource(path);
		if (imgURL != null) {
			return new ImageIcon(imgURL, description);
		} else {
			System.err.println("Couldn't find file: " + path);
			return null;
		}
	}

	// addComponents method
	public void addComponents() {
		pack();
		super.addComponents();

		// set location of each panel
		JLabel pic = new JLabel(createImageIcon("manee.jpg", ""));
		picPanel = new JPanel();
		patientFormV6Panel = new JPanel(new BorderLayout());
		picPanel.add(pic);

		patientFormV6Panel.add(menuBar, BorderLayout.NORTH);
		patientFormV6Panel.add(picPanel, BorderLayout.CENTER);
		patientFormV6Panel.add(patientv3Panel, BorderLayout.SOUTH);
		add(patientFormV6Panel);

		// add image to item
		openItem.setIcon(iconOpen);
		saveItem.setIcon(iconSave);
		exitItem.setIcon(iconExit);

	}

}
