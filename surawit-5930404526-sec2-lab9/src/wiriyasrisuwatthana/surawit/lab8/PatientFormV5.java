package wiriyasrisuwatthana.surawit.lab8;
/*	
 * This is program as same as PatientFormV4 but can change color and size font
 * 
 *	@author  Surawit Wiriyasrisuwatthana 
 * 	
 * 	@version 1.0
 * 
 *  23/03/2017
 * 
 */
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class PatientFormV5 extends PatientFormV4 implements ActionListener, ItemListener {

	// generated UID
	private static final long serialVersionUID = -7289295930559208344L;

	// title constructor
	public PatientFormV5(String title) {
		super(title);
	}
	
	// Variable
	protected JMenu colorsubMenu,sizesubMenu;
	protected JMenuItem bluecolorItem,greencolorItem,redcolorItem,customcolorItem;
	protected JMenuItem size16Item,size20Item,size24Item,customsizeItem;
	protected Font size16,size20,size24;
	

	
	// addComponents Method
	@Override
	protected void addComponents(){
			super.addComponents();
			
		// Font
		size16 = new Font ("SansSerif",Font.BOLD,16);
		size20 = new Font ("SansSerif",Font.BOLD,20);
		size24 = new Font ("SansSerif",Font.BOLD,24);
			
		// color
		colorsubMenu = new JMenu("Color");
		bluecolorItem = new JMenuItem("Blue");
		greencolorItem = new JMenuItem("Green");
		redcolorItem = new JMenuItem("Red");
		customcolorItem = new JMenuItem("Custom...");
		
		// size
		sizesubMenu = new JMenu("Size");
		size16Item = new JMenuItem("16");
		size20Item = new JMenuItem("20");
		size24Item = new JMenuItem("24");
		customsizeItem = new JMenuItem("Custom...");
		
		// remove color and sizeItem
		configMenu.remove(colorItem);
		configMenu.remove(sizeItem);
		
		
		colorsubMenu.add(bluecolorItem);
		colorsubMenu.add(greencolorItem);
		colorsubMenu.add(redcolorItem);
		colorsubMenu.add(customcolorItem);
		configMenu.add(colorsubMenu);
		
		sizesubMenu.add(size16Item);
		sizesubMenu.add(size20Item);
		sizesubMenu.add(size24Item);
		sizesubMenu.add(customsizeItem);
		configMenu.add(sizesubMenu);
		
	}
	
	// listener
	protected void addListeners() {
		super.addListeners();
		
		// color actionListener
		bluecolorItem.addActionListener(this);
		greencolorItem.addActionListener(this);
		redcolorItem.addActionListener(this);
	    // size actionListner
	    size16Item.addActionListener(this);
	    size20Item.addActionListener(this);
	    size24Item.addActionListener(this);
	}
	
	// actionperformed method
	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
		Object src = event.getSource();
		if (src == bluecolorItem) {
			handleblueColor();
		} else if (src == greencolorItem) {
			handlegreenColor();
		} else if (src == redcolorItem) {
			handleredColor();
		} else if (src == size16Item) {
			handlesize16();
		}else if (src == size20Item) {
			handlesize20();
		}else if (src == size24Item) {
			handlesize24();
		}
	}
	
	// set color
	private void handleblueColor() {
		nameTextField.setForeground(Color.BLUE);
		birthDateTextField.setForeground(Color.BLUE);
		weightTextField.setForeground(Color.BLUE);
		heightTextField.setForeground(Color.BLUE);
		addressText.setForeground(Color.BLUE);
	}
	private void handlegreenColor() {
		nameTextField.setForeground(Color.GREEN);
		birthDateTextField.setForeground(Color.GREEN);
		weightTextField.setForeground(Color.GREEN);
		heightTextField.setForeground(Color.GREEN);
		addressText.setForeground(Color.GREEN);
	}
	private void handleredColor() {
		nameTextField.setForeground(Color.RED);
		birthDateTextField.setForeground(Color.RED);
		weightTextField.setForeground(Color.RED);
		heightTextField.setForeground(Color.RED);
		addressText.setForeground(Color.RED);
	}
	
	// set size
	private void handlesize16() {
		nameTextField.setFont(size16);
		birthDateTextField.setFont(size16);
		weightTextField.setFont(size16);
		heightTextField.setFont(size16);
		addressText.setFont(size16);
	}
	private void handlesize20() {
		nameTextField.setFont(size20);
		birthDateTextField.setFont(size20);
		weightTextField.setFont(size20);
		heightTextField.setFont(size20);
		addressText.setFont(size20);
	}
	private void handlesize24() {
		nameTextField.setFont(size24);
		birthDateTextField.setFont(size24);
		weightTextField.setFont(size24);
		heightTextField.setFont(size24);
		addressText.setFont(size24);
	}

	// createAndShowGUI Method
	public static void createAndShowGUI(){
		PatientFormV5 patientForm5 = new PatientFormV5("Patient Form V5");
		patientForm5.addComponents();
		patientForm5.setFrameFeatures();
		patientForm5.addListeners();		
}
	
	// main Method
	public static void main(String[] args) {
	SwingUtilities.invokeLater(new Runnable() {
		public void run() {
			createAndShowGUI();
		}
	});
}
}

