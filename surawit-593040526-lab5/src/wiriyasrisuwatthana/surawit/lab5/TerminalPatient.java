package wiriyasrisuwatthana.surawit.lab5;

import java.time.LocalDate;

import wiriyasrisuwatthana.surawit.lab4.Gender;
import wiriyasrisuwatthana.surawit.lab4.Patient;

public class TerminalPatient extends PatientV2 {
	private String terminalDisease;
	private LocalDate firstdiagnosed;

	public TerminalPatient(String name, String birthdayStr, Gender gender, double weight, int height,
			String terminalDisease, String firstdiagnosed) {
		super(name, birthdayStr, gender, weight, height);
		this.terminalDisease = terminalDisease;
		this.firstdiagnosed = LocalDate.parse(firstdiagnosed, germanFormatter);
	}

	public String getTerminalDisease() {
		return terminalDisease;
	}

	public void setTerminalDisease(String terminalDisease) {
		this.terminalDisease = terminalDisease;
	}

	public LocalDate getFirstdiagnosed() {
		return firstdiagnosed;
	}

	public void setFirstdiagnosed(LocalDate firstdiagnosed) {
		this.firstdiagnosed = firstdiagnosed;
	}

	@Override
	public String toString() {
		return "TerminalPatient [" + terminalDisease + ", " + firstdiagnosed + ", " + super.toString() + "]";
	}

	public void patientReport() {
		System.out.println("You have terminal illness");
	}
}
