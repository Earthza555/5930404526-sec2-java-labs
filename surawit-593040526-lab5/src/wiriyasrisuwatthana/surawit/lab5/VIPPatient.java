package wiriyasrisuwatthana.surawit.lab5;

import wiriyasrisuwatthana.surawit.lab4.Gender;

public class VIPPatient extends PatientV2 {
	private double totalDonation;
	public String passPhrase;

	public VIPPatient(String name, String birthdayStr, Gender gender, double weight, int height, double totalDonation,
			String passPhrase) {
		super(encrypt(name), birthdayStr, gender, weight, height);
		this.totalDonation = totalDonation;
		this.passPhrase = passPhrase;

	}

	public double getTotalDonation() {
		return totalDonation;
	}

	public void setTotalDonation(double totalDonation) {
		this.totalDonation = totalDonation;
	}

	public double donate(double donate) {
		totalDonation = totalDonation + donate;
		return totalDonation;
	}

	public static String encrypt(String message) {
		String newMessage = "";
		char temp;

		for (int i = 0; i < message.length(); i++) {
			temp = message.charAt(i);
			temp += 7;
			if (temp > 'z') {
				temp -= 26;

			}
			newMessage += temp;
		}
		return newMessage;

	}

	public String decrypt(String message) {
		String newMessage = "";
		char temp;

		for (int i = 0; i < message.length(); i++) {
			temp = message.charAt(i);
			temp -= 7;
			if (temp < 'a') {
				temp += 26;

			}
			newMessage += temp;
		}
		return newMessage;

	}

	public void setName(String name) {
		super.setName(encrypt(name));
	}

	public String getPassPhrase() {
		return passPhrase;
	}

	public void setPassPhrase(String passPhrase) {
		this.passPhrase = passPhrase;
	}

	public String getVIPName(String passPhrase) {
		if (passPhrase.equals(this.passPhrase)) {
			return decrypt(this.getName());
		} else {
			return "Wrong passphrase, please try again";
		}

	}

	public void patientReport() {
		System.out.println("Your record is private");
	}

	@Override
	public String toString() {
		return "VIPPatient [" + totalDonation + ", " + super.toString();
	}

}
