package wiriyasrisuwatthana.surawit.lab5;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;
import wiriyasrisuwatthana.surawit.lab4.*;

//constructor which accepts values for the fields name,birthdate,gender,height,weight

public class PatientV3 extends SickPeople {

	private String name;
	private LocalDate birthdate;
	private Gender gender;
	private double weight;
	private int height;
	public DateTimeFormatter germanFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM)
			.withLocale(Locale.GERMAN);

	public PatientV3(String name, String birthdayStr, Gender gender, double weight, int height) {
		super();
		this.name = name;
		this.birthdate = LocalDate.parse(birthdayStr, germanFormatter);
		this.gender = gender;
		this.weight = weight;
		this.height = height;

		// methods to set and get each information
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDate getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(LocalDate birthdate) {
		this.birthdate = birthdate;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	@Override
	public String toString() {
		return "Patient [" + name + ", " + birthdate + ", " + gender + ", " + weight + " kg. " + ", " + height + " cm. "
				+ "]";
	}

	public void seeDoctor() {
		System.out.println("go see the Doctor.");
	}

}
