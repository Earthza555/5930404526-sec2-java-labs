package wiriyasrisuwatthana.surawit.lab5;

import wiriyasrisuwatthana.surawit.lab4.*;

public class PatientV2 extends PatientV3 {

	public PatientV2(String name, String birthdayStr, Gender gender, double weight, int height) {
		super(name, birthdayStr, gender, weight, height);

	}

	public void patientReport() {
		System.out.println("You need medical attention.");
	}
}
