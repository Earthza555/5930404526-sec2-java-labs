//interface class
package wiriyasrisuwatthana.surawit.lab5;

public interface UnderLegalAge {
	public void askPermission();

	public void askPermission(String legalGuardianName);

}
