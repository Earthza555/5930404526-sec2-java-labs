package wiriyasrisuwatthana.surawit.lab5;

import java.time.LocalDate;

import wiriyasrisuwatthana.surawit.lab4.Gender;

public class TerminalPatientV2 extends PatientV3 implements HasInsurance, UnderLegalAge {
	private String terminalDisease;
	private LocalDate firstdiagnosed;

	public TerminalPatientV2(String name, String birthdayStr, Gender gender, double weight, int height,
			String terminalDisease, String firstdiagnosed) {
		super(name, birthdayStr, gender, weight, height);
		this.terminalDisease = terminalDisease;
		this.firstdiagnosed = LocalDate.parse(firstdiagnosed, germanFormatter);
	}

	public String getTerminalDisease() {
		return terminalDisease;
	}

	public void setTerminalDisease(String terminalDisease) {
		this.terminalDisease = terminalDisease;
	}

	public LocalDate getFirstdiagnosed() {
		return firstdiagnosed;
	}

	public void setFirstdiagnosed(LocalDate firstdiagnosed) {
		this.firstdiagnosed = firstdiagnosed;
	}

	@Override
	public String toString() {
		return "TerminalPatient [" + terminalDisease + ", " + firstdiagnosed + ", " + super.toString() + "]";
	}

	public void patientReport() {
		System.out.println("You have terminal illness");
	}

	@Override
	public void pay() {
		System.out.println("pay the accident bill with insurance");

	}

	@Override
	public void pay(double amount) {
		System.out.println("pay " + amount + " baht for his hospital visit by insurance.");

	}

	@Override
	public void askPermission() {
		System.out.println("ask parents for permission to cure him in an accident.");

	}

	@Override
	public void askPermission(String legalGuardianName) {
		System.out.println("ask " + legalGuardianName + " for permission to treat with Chemo");

	}
}
