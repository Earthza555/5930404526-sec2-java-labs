// interface class
package wiriyasrisuwatthana.surawit.lab5;

public interface HasPet {
	public void feedPet();		// interface method

	public void playWithPet();	// interface method

}
