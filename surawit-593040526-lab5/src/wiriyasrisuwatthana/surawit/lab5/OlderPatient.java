package wiriyasrisuwatthana.surawit.lab5;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import wiriyasrisuwatthana.surawit.lab4.Gender;

public class OlderPatient {
	public static void main(String arg[]) {
		PatientV2 piti = new AccidentPatient("piti", "12.01.2000", Gender.MALE, 65.5, 169, "Car accident", true);
		PatientV2 weera = new TerminalPatient("weera", "15.02.2000", Gender.MALE, 72, 172, "Cancer", "01.01.2017");
		PatientV2 duangjai = new VIPPatient("duangjai", "21.05.2001", Gender.FEMALE, 47.5, 154, 1000000, "mickeymouse");
		AccidentPatient petch = new AccidentPatient("petch", "15.10.1999", Gender.MALE, 68, 170, "FIre accident", true);
		isOlder(piti, weera);
		isOlder(piti, duangjai);
		isOlder(piti, petch);
		isOldest(piti, weera, duangjai);
		isOldest(piti, duangjai, petch);
	}

	public static void isOlder(PatientV2 name1, PatientV2 name2) {

		LocalDate birthdate1 = name1.getBirthdate();
		LocalDate birthdate2 = name2.getBirthdate();
		LocalDate today = LocalDate.now();
		long dayTonow1 = ChronoUnit.DAYS.between(birthdate1, today);
		long dayTonow2 = ChronoUnit.DAYS.between(birthdate2, today);

		if (dayTonow1 > dayTonow2) {
			System.out.println(((name1 instanceof VIPPatient)?((VIPPatient)name1).getVIPName("mickeymouse"):name1.getName()) + " is older than " + ((name2 instanceof VIPPatient)?((VIPPatient)name2).getVIPName("mickeymouse"):name2.getName() + "."));
		} else {
			System.out.println(((name1 instanceof VIPPatient)?((VIPPatient)name1).getVIPName("mickeymouse"):name1.getName()) + " is not older than " + ((name2 instanceof VIPPatient)?((VIPPatient)name2).getVIPName("mickeymouse"):name2.getName() + "."));
		}
	}
// method
	public static void isOldest(PatientV2 name1, PatientV2 name2, PatientV2 name3) {
		LocalDate birthdate1 = name1.getBirthdate();
		LocalDate birthdate2 = name2.getBirthdate();
		LocalDate birthdate3 = name3.getBirthdate();
		LocalDate today = LocalDate.now();
		long dayTonow1 = ChronoUnit.DAYS.between(birthdate1, today);
		long dayTonow2 = ChronoUnit.DAYS.between(birthdate2, today);
		long dayTonow3 = ChronoUnit.DAYS.between(birthdate3, today);
//used this way because couldn't do anyway		
		if (dayTonow1 > dayTonow2 && dayTonow1 > dayTonow3) {
			System.out.println(((name1 instanceof VIPPatient)?((VIPPatient)name1).getVIPName("mickeymouse"):name1.getName()) + " is the oldest among " + ((name1 instanceof VIPPatient)?((VIPPatient)name1).getVIPName("mickeymouse"):name1.getName()) + ", " + ((name2 instanceof VIPPatient)?((VIPPatient)name2).getVIPName("mickeymouse"):name2.getName() + ".")
					+ " and " + ((name3 instanceof VIPPatient)?((VIPPatient)name3).getVIPName("mickeymouse"):name3.getName() + ".")); 
		} else if (dayTonow2 > dayTonow1 && dayTonow2 > dayTonow3) {
			System.out.println(name2.getName() + " is the oldest among " +((name1 instanceof VIPPatient)?((VIPPatient)name1).getVIPName("mickeymouse"):name1.getName()) + ", " + ((name2 instanceof VIPPatient)?((VIPPatient)name2).getVIPName("mickeymouse"):name2.getName() + ".")
					+ " and " + ((name1 instanceof VIPPatient)?((VIPPatient)name1).getVIPName("mickeymouse"):name1.getName() + "."));
		} else {
			System.out.println(name3.getName() + " is the oldest among " + ((name1 instanceof VIPPatient)?((VIPPatient)name1).getVIPName("mickeymouse"):name1.getName()) + ", " + ((name2 instanceof VIPPatient)?((VIPPatient)name2).getVIPName("mickeymouse"):name2.getName() + ".")
					+ " and " + ((name1 instanceof VIPPatient)?((VIPPatient)name1).getVIPName("mickeymouse"):name1.getName() + "."));

		}

	}
}
