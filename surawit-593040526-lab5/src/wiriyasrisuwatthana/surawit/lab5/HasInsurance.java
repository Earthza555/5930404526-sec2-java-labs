// created interface class.
package wiriyasrisuwatthana.surawit.lab5;

public interface HasInsurance {
	public void pay(); // interface method

	public void pay(double amount);	// interface method

}
