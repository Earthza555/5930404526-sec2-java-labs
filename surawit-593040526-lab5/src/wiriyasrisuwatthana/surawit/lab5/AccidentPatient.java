package wiriyasrisuwatthana.surawit.lab5;

import wiriyasrisuwatthana.surawit.lab4.Gender;

public class AccidentPatient extends PatientV2 {
	private String typeOfAccident;
	private boolean isInICU;

// constructor
	public AccidentPatient(String name, String birthdayStr, Gender gender, double weight, int height,
			String typeOfAccident, Boolean isInICU) {
		super(name, birthdayStr, gender, weight, height);
		this.typeOfAccident = typeOfAccident;
		this.isInICU = isInICU;
	}

// get and set 
	public String getTypeOfAccident() {
		return typeOfAccident;
	}

	public void setTypeOfAccident(String typeOfAccident) {
		this.typeOfAccident = typeOfAccident;
	}

	public boolean isInICU() {
		return isInICU;
	}

	public void setInICU(Boolean isInICU) {
		this.isInICU = isInICU;

	}

	@Override
	public String toString() {
		return "AccidentPatient [" + typeOfAccident + ", " + (isInICU ? "In ICU" : "is out of ICU") + "], "
				+ super.toString() + "]";
	}
//patientReport method 
	public void patientReport() {
		System.out.println("You were in an accident.");
	}

}
