package wiriyasrisuwatthana.surawit.lab5;

import wiriyasrisuwatthana.surawit.lab4.Gender;

public class VIPPatientV2 extends PatientV3 implements HasInsurance, HasPet {
	private double totalDonation;
	public String passPhrase;
//constructor
	public VIPPatientV2(String name, String birthdayStr, Gender gender, double weight, int height, double totalDonation,
			String passPhrase) {
		super(encrypt(name), birthdayStr, gender, weight, height);
		this.totalDonation = totalDonation;
		this.passPhrase = passPhrase;

	}
//get and set
	public double getTotalDonation() {
		return totalDonation;
	}

	public void setTotalDonation(double totalDonation) {
		this.totalDonation = totalDonation;
	}

	public double donate(double donate) {
		totalDonation = totalDonation + donate;
		return totalDonation;
	}
//encrypt method
	public static String encrypt(String message) {
		String newMessage = "";
		char temp;

		for (int i = 0; i < message.length(); i++) {
			temp = message.charAt(i);
			temp += 7;
			if (temp > 'z') {
				temp -= 26;

			}
			newMessage += temp;
		}
		return newMessage;

	}
//decrypt method
	public String decrypt(String message) {
		String newMessage = "";
		char temp;

		for (int i = 0; i < message.length(); i++) {
			temp = message.charAt(i);
			temp -= 7;
			if (temp < 'a') {
				temp += 26;

			}
			newMessage += temp;
		}
		return newMessage;

	}
// set name method
	public void setName(String name) {
		super.setName(encrypt(name));
	}
// get PassPhrase method
	public String getPassPhrase() {
		return passPhrase;
	}
// set PassPhrase method
	public void setPassPhrase(String passPhrase) {
		this.passPhrase = passPhrase;
	}
// get VIPName method
	public String getVIPName(String passPhrase) {
		if (passPhrase.equals(this.passPhrase)) {
			return decrypt(this.getName());
		} else {
			return "Wrong passphrase, please try again";
		}

	}
// patientReport method
	public void patientReport() {
		System.out.println("Your record is private");
	}
// override for complete 
	@Override
	public String toString() {
		return "VIPPatient [" + totalDonation + ", " + super.toString();
	}

	@Override
	public void feedPet() {
		System.out.println("feed the pet");

	}

	@Override
	public void playWithPet() {
		System.out.println("play with pet.");

	}

	@Override
	public void pay() {
		System.out.println("pay the accident bill with insurance");
	}

	@Override
	public void pay(double amount) {
		System.out.println("pay" + amount + "baht for his hospital visit by insurance.");

	}

}
