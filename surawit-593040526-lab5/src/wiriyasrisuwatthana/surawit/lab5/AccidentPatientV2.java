package wiriyasrisuwatthana.surawit.lab5;

import wiriyasrisuwatthana.surawit.lab4.Gender;

public class AccidentPatientV2 extends PatientV3 implements HasInsurance, UnderLegalAge {

	private String typeOfAccident;
	private boolean isInICU;
// constructor
	public AccidentPatientV2(String name, String birthdayStr, Gender gender, double weight, int height,
			String typeOfAccident, Boolean isInICU) {
		super(name, birthdayStr, gender, weight, height);			//extends from PatientV3
		this.typeOfAccident = typeOfAccident;
		this.isInICU = isInICU;
	}
// get and set
	public String getTypeOfAccident() {
		return typeOfAccident;
	}

	public void setTypeOfAccident(String typeOfAccident) {
		this.typeOfAccident = typeOfAccident;
	}

	public boolean isInICU() {
		return isInICU;
	}

	public void setInICU(Boolean isInICU) {
		this.isInICU = isInICU;

	}
// for complete output of KhonKaenPatientV4
	@Override
	public String toString() {
		return "AccidentPatient [" + typeOfAccident + ", " + (isInICU ? "In ICU" : "is out of ICU") + "], "
				+ super.toString() + "]";
	}

	public void patientReport() {
		System.out.println("You were in an accident.");
	}

	@Override
	public void askPermission() {
		System.out.println("ask parents for permission to cure him in an accident.");

	}

	@Override
	public void askPermission(String legalGuardianName) {
		System.out.println("ask " + legalGuardianName + " for permission to treat with Chemo");

	}

	@Override
	public void pay() {
		System.out.println("pay the accident bill with insurance");

	}

	@Override
	public void pay(double amount) {
		System.out.println("pay" + amount + "baht for his hospital visit by insurance.");

	}

}
