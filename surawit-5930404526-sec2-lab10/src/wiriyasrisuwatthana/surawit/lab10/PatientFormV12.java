package wiriyasrisuwatthana.surawit.lab10;

/**
 * This program is develop from PatientV11 and create function remove and search
 * 
 * @author Surawit Wiriyasrisuwatthana
 * 
 * @version 1.0
 * 
 */
import java.awt.event.ActionEvent;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

public class PatientFormV12 extends PatientFormV11 {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1853670942813863437L;

	// constructor
	public PatientFormV12(String string) {
		super(string);

	}

	// main method
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();

			}
		});

	}

	// addComponents method from superclass
	public void addComponents() {
		pack();
		super.addComponents();
	}

	// addListeners Method from super class, add searchMenuItem & removeMenuItem
	protected void addListeners() {
		super.addListeners();
		searchMI.addActionListener(this);
		removeMI.addActionListener(this);
	}

	// createAndShowGUI Method
	public static void createAndShowGUI() {
		PatientFormV12 patientForm12 = new PatientFormV12("Patient Form V12");
		patientForm12.addComponents();
		patientForm12.setFrameFeatures();
		patientForm12.addListeners();
	}

	// actionPerformed Method
	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
		Object src = event.getSource();
		if (src == searchMI) {
			String searchName = JOptionPane.showInputDialog("Please enter the patient name");
			searchPatient(searchName);
		} else if (src == removeMI) {
			String removeName = JOptionPane.showInputDialog("Please enter the patient name to remove");
			removePatient(removeName);
		}
	}

	// removePatient method working when click in removeMenuItem
	public void removePatient(String removeName) {
		for (int i = 0; i < profileList.size(); i++) {
			if (profileList.get(i).getName().equalsIgnoreCase(removeName)) {
				profileList.remove(i);
				displayPatient();
			}
		}

	}

	// searchPatient method working when click in searchMenuItem
	public void searchPatient(String searchName) {
		for (int i = 0; i < profileList.size(); i++) {
			if (profileList.get(i).getName().equalsIgnoreCase(searchName)) {
				JOptionPane.showMessageDialog(this, profileList.get(i).toString());
			} else if (!profileList.get(i).getName().equalsIgnoreCase(searchName)) {
				JOptionPane.showMessageDialog(this, searchName + " is not found");
			}
		}

	}

}
