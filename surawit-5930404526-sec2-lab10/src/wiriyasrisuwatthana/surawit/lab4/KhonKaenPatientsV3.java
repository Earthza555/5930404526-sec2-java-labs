package wiriyasrisuwatthana.surawit.lab4;
/**
 * KhonKaenPatientsV3
 * 
 * This program is to illustrate the examples of how to create objects in class
 * InPatient
 * 
 * @author Surawit Wiriyasrisuwatthana
 * 
 * @version 1.0
 *
 *          09-02-2017
 *
 */

public class KhonKaenPatientsV3 {
	public static void main(String[] args) {
		Patient manee = new InPatient("Manee","01.12.1980",Gender.FEMALE,60,150,"20.01.2017","29.01.2017");
		Patient mana = new OutPatient("Mana","22.04.1981",Gender.MALE,70,160,"23.01.2017");
		Patient chujai = new Patient("Chujai","03.03.1980",Gender.FEMALE,41.5,175);
		System.out.println(manee);
		System.out.println(mana);
		System.out.println(chujai); 
		if(isTaller(manee,mana)){
			System.out.println(manee.getName() + " is taller than " + mana.getName());
		} else {
			System.out.println(mana.getName() + " is taller than " + manee.getName());
		}

	}
	
//isTaller method
	public static boolean isTaller(Patient people1 ,Patient people2 ){
		if (people1.getHeight() > people2.getHeight()){
			return true;
		}
		else {
			return false;
		}
	}
}
