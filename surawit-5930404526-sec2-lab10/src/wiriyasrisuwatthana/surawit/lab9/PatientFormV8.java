package wiriyasrisuwatthana.surawit.lab9;

/**
 * This program is add Mnemonic and setAccelerator key 
 * 
 * @author Surawit Wiriyasrisuwatthana
 * 
 * @version 1.0
 * 
 */
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

public class PatientFormV8 extends PatientFormV7 {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5926756406158736661L;

	public PatientFormV8(String string) {
		super(string);

	}

	// listeners
	protected void addListeners() {
		super.addListeners();

		configMenu.addActionListener(this);
		fileMenu.addActionListener(this);
		colorsubMenu.addActionListener(this);
		sizeItem.addActionListener(this);
		bluecolorItem.addActionListener(this);
		greencolorItem.addActionListener(this);
		redcolorItem.addActionListener(this);
		sizeItem.addActionListener(this);
		size16Item.addActionListener(this);
		size20Item.addActionListener(this);
		size24Item.addActionListener(this);

	}

	// addComponents method
	public void addComponents() {
		pack();
		super.addComponents();

		// set Mnemonic
		fileMenu.setMnemonic('F');
		newItem.setMnemonic('N');
		openItem.setMnemonic('O');
		saveItem.setMnemonic('S');
		exitItem.setMnemonic('X');
		configMenu.setMnemonic('C');
		colorsubMenu.setMnemonic('L');
		bluecolorItem.setMnemonic('B');
		greencolorItem.setMnemonic('G');
		redcolorItem.setMnemonic('R');
		customcolorItem.setMnemonic('U');
		sizeItem.setMnemonic('Z');
		size16Item.setMnemonic('6');
		size20Item.setMnemonic('0');
		size24Item.setMnemonic('4');
		customsizeItem.setMnemonic('M');

		// set Accelerator key
		newItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK));
		openItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.CTRL_MASK));
		saveItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
		exitItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, ActionEvent.CTRL_MASK));

		bluecolorItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B, ActionEvent.CTRL_MASK));
		greencolorItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G, ActionEvent.CTRL_MASK));
		redcolorItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, ActionEvent.CTRL_MASK));
		customcolorItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_U, ActionEvent.CTRL_MASK));
		size16Item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_6, ActionEvent.CTRL_MASK));
		size20Item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_0, ActionEvent.CTRL_MASK));
		size24Item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_4, ActionEvent.CTRL_MASK));
		customsizeItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_M, ActionEvent.CTRL_MASK));

	}

	// createAndShowGUI Method
	public static void createAndShowGUI() {
		PatientFormV8 patientForm8 = new PatientFormV8("Patient Form V8");
		patientForm8.addComponents();
		patientForm8.setFrameFeatures();
		patientForm8.addListeners();
	}

	// main method
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
