package wiriyasrisuwatthana.surawit.lab9;

/**
 * This program is add open dialog, save dialog and custom colorchooser
 * 
 * @author Surawit Wiriyasrisuwatthana
 * 
 * @version 1.0
 * 
 */
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.io.File;
import javax.swing.JColorChooser;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

public class PatientFormV9 extends PatientFormV8 {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1043984503661071228L;

	protected JFileChooser fc = new JFileChooser();

	public PatientFormV9(String string) {
		super(string);

	}

	@Override
	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
		Object src = event.getSource();
		if (src == openItem) {
			openDialog();
		} else if (src == saveItem) {
			saveDialog();
		} else if (src == customcolorItem) {
			colorDialog();
		}
	}
	
	// listeners
	protected void addListeners() {
		super.addListeners();
		newItem.addActionListener(this);
		openItem.addActionListener(this);
		saveItem.addActionListener(this);
		exitItem.addActionListener(this);
		customsizeItem.addActionListener(this);
		customcolorItem.addActionListener(this);
	}

	// addcomponent form patientformv8
	public void addComponents() {
		pack();
		super.addComponents();

	}

	// createAndShowGUI Method
	public static void createAndShowGUI() {
		PatientFormV9 patientForm9 = new PatientFormV9("Patient Form V8");
		patientForm9.addComponents();
		patientForm9.setFrameFeatures();
		patientForm9.addListeners();
	}

	// main method
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	// open dialog method for show when choose openItem
	protected void openDialog() {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
		int result = fileChooser.showOpenDialog(this);
		if (result == JFileChooser.APPROVE_OPTION) {
			File selectedFile = fileChooser.getSelectedFile();
			JOptionPane.showMessageDialog(this, "You have opened file " + selectedFile.getName());
		} else {
			JOptionPane.showMessageDialog(this, "Open command is cancelled ");
		}
	}

	// save dialog method for show when choose saveItem
	protected void saveDialog() {
		JFileChooser fileChooser = new JFileChooser();

		int result = fileChooser.showSaveDialog(this);
		if (result == JFileChooser.APPROVE_OPTION) {
			File selectedFile = fileChooser.getSelectedFile();
			JOptionPane.showMessageDialog(this, "You have saved file " + selectedFile.getName());
		} else {
			JOptionPane.showMessageDialog(this, "Save command is cancelled ");
		}
	}

	// colorDialog method for show when choose custom in color
	protected void colorDialog() {
		Color previousColor = addressText.getForeground();
		Color color = JColorChooser.showDialog(this, "Select a color", previousColor);
		if (color != null)
			nameTextField.setForeground(color);
		birthDateTextField.setForeground(color);
		weightTextField.setForeground(color);
		heightTextField.setForeground(color);
		addressText.setForeground(color);
	}
}
