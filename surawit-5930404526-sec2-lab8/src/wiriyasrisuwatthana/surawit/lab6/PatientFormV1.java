package wiriyasrisuwatthana.surawit.lab6;

import javax.swing.*;
import java.awt.*;

/*	
 * This is program about textfield and add Cancel & OK button
 * 
 *	@author  Surawit Wiriyasrisuwatthana 
 * 	
 * 	@version 1.0
 * 
 */

public class PatientFormV1 extends MySimpleWindow {

	private static final long serialVersionUID = 1L;

	// JLabel variable
	protected JLabel nameLabel;
	protected JLabel birthDateLabel;
	protected JLabel weightLabel;
	protected JLabel heightLabel;
	protected final static int TEXT_LENGTH = 15;

	// JTextField variable
	protected JTextField nameTextField, birthDateTextField, weightTextField, heightTextField;
	
	// JPabel
	protected JPanel inputPanel, mixPanel;

	public PatientFormV1(String string) {
		super(string);

	}

	@Override
	protected void addComponents() {
		
		//Button form MySimpleWindow
		super.addComponents();
		
		//Label
		nameLabel = new JLabel("Name:");
		birthDateLabel = new JLabel("Birthdate");
		weightLabel = new JLabel("Weight(kg.)");
		heightLabel = new JLabel("Height(metre)");
		
		//TextField
		nameTextField = new JTextField(TEXT_LENGTH);
		birthDateTextField = new JTextField(TEXT_LENGTH);
		weightTextField = new JTextField(TEXT_LENGTH);
		heightTextField = new JTextField(TEXT_LENGTH);

		//Panel
		inputPanel = new JPanel();
		mixPanel = new JPanel();
		mixPanel.setLayout(new BorderLayout());
		
		//add to Panel
		inputPanel.setLayout(new GridLayout(4, 2));
		inputPanel.add(nameLabel);
		inputPanel.add(nameTextField);
		inputPanel.add(birthDateLabel);
		inputPanel.add(birthDateTextField);
		birthDateTextField.setToolTipText("ex.22.02.2000");	
		inputPanel.add(weightLabel);
		inputPanel.add(weightTextField);
		inputPanel.add(heightLabel);
		inputPanel.add(heightTextField);

		mixPanel.add(inputPanel, BorderLayout.NORTH);
		mixPanel.add(mswPanel, BorderLayout.SOUTH);
		
		add(mixPanel);

	}
	
	// createAndShowGUI Method
	public static void createAndShowGUI() {
		PatientFormV1 patientForm1 = new PatientFormV1("Patient Form V1");
		patientForm1.addComponents();
		patientForm1.setFrameFeatures();
	}
	
	// main method
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
