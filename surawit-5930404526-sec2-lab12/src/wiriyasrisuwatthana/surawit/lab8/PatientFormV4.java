package wiriyasrisuwatthana.surawit.lab8;
/*	
 * This is program about using dialog for showing information
 * 
 *	@author  Surawit Wiriyasrisuwatthana 
 * 	
 * 	@version 1.0
 * 
 *  23/03/2017
 * 
 */

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.SwingUtilities;

import wiriyasrisuwatthana.surawit.lab6.PatientFormV3;

public class PatientFormV4 extends PatientFormV3 implements ActionListener, ItemListener {
	
	protected String showingMessage ;
	public PatientFormV4(String string) {
		super(string);

	}

	// actionPerformed method for choose way to do
	public void actionPerformed(ActionEvent event) {
		Object src = event.getSource();
		if (src == okButton) {
			handleOKButton();
		} else if (src == cancelButton) {
			handleCancelButton();
		} else if (src == typeComboBox)
			typeDialog();

	}

	// addListeners method for register
	protected void addListeners() {
		okButton.addActionListener(this);
		cancelButton.addActionListener(this);
		typeComboBox.addActionListener(this);
		maleButton.addItemListener(this);
		femaleButton.addItemListener(this);

	}

	// if click OK button will do
	public void handleOKButton() {

		 showingMessage = "Name = " + nameTextField.getText() + " Birthdate = " + birthDateTextField.getText()
				+ " Weight = " + weightTextField.getText() + " Height = " + heightTextField.getText() + "\n"
				+ "Gender = " + (maleButton.isSelected() ? "Male" : (femaleButton.isSelected() ? "Female" : "")) + "\n"
				+ "Address = " + addressText.getText() + "\n" + "Type = " + typeComboBox.getSelectedItem();

		JOptionPane.showMessageDialog(null, showingMessage, "Message", JOptionPane.PLAIN_MESSAGE);

	}

	// if click Cancel button will clear textfield
	public void handleCancelButton() {
		nameTextField.setText("");
		birthDateTextField.setText("");
		weightTextField.setText("");
		heightTextField.setText("");
		addressText.setText("");
	}

	// createAndShowGUI method
	public static void createAndShowGUI() {
		PatientFormV4 patientForm4 = new PatientFormV4("Patient Form V4");
		patientForm4.addComponents();
		patientForm4.addListeners();
		patientForm4.setFrameFeatures();

	}

	// main method
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	@Override
	public void itemStateChanged(ItemEvent e) {

		// genderDialog for use male or female;
		Object src = e.getSource();
		if (((JRadioButton) src).isSelected()) {
			if (src == maleButton) {
				genderDialog("Male");
			} else if (src == femaleButton) {
				genderDialog("Female");
			}
		}
	}

	// genderDiallog method for showing dialog that fix location
	public void genderDialog(String selectedGender) {

		JOptionPane pane = new JOptionPane("Your Gender type is now changed to " + selectedGender);
		JDialog jDialog = pane.createDialog(this, "Gender Info");
		jDialog.setLocation(getX(), getY() + getHeight() + 20);
		jDialog.setVisible(true);

	}

	// typeDialog method having Outpatient or Inpatient
	public void typeDialog() {
		if (typeComboBox.getSelectedItem().equals("Outpatient")) {
			JOptionPane.showMessageDialog(null, "Your patient type is now changed to Outpatient", "Message",
					JOptionPane.PLAIN_MESSAGE);
		} else if (typeComboBox.getSelectedItem().equals("Inpatient")) {

			JOptionPane.showMessageDialog(null, "Your patient type is now changed to Inpatient", "Message",
					JOptionPane.PLAIN_MESSAGE);
		}
	}
}
