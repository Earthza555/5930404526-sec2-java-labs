package wiriyasrisuwatthana.surawit.lab12;

import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import wiriyasrisuwatthana.surawit.lab10.PatientFormV12;
import wiriyasrisuwatthana.surawit.lab4.Gender;
import wiriyasrisuwatthana.surawit.lab4.Patient;

public class PatientFormV13 extends PatientFormV12 {

	public PatientFormV13(String string) {
		super(string);

	}

	// main method
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();

			}
		});

	}

	// addComponents method from superclass
	public void addComponents() {
		pack();
		super.addComponents();
	}

	// addListeners Method from super class, add searchMenuItem & removeMenuItem
	protected void addListeners() {
		super.addListeners();

	}

	// createAndShowGUI Method
	public static void createAndShowGUI() {
		PatientFormV13 patientForm13 = new PatientFormV13("Patient Form V13");
		patientForm13.addComponents();
		patientForm13.setFrameFeatures();
		patientForm13.addListeners();
	}

	

	@Override
	// save dialog method for show when choose saveItem
	protected void saveDialog() {

		int result = fc.showSaveDialog(this);
		if (result == JFileChooser.APPROVE_OPTION) {
			File selectedFile = fc.getSelectedFile();
			JOptionPane.showMessageDialog(this, "You have saved file " + selectedFile.getName());

			try {
				String filePath = fc.getSelectedFile().toString();
				PrintWriter writer = new PrintWriter(filePath);

				for (int i = 0; i < profileList.size(); i++) {
					writer.println("Name = " + profileList.get(i).getName());
					writer.println("Birthdate = " + profileList.get(i).getBirthdate());
					writer.println("Weight = " + profileList.get(i).getWeight());
					writer.println("Height = " + profileList.get(i).getHeight());
					writer.println("Gender = " + profileList.get(i).getGender());
				}
				writer.close();
			} catch (IOException e) {

				e.printStackTrace();
			}
		} else {
			JOptionPane.showMessageDialog(this, "Save command is cancelled ");
		}
	}

	@Override
	// open dialog method for show when choose openItem
	protected void openDialog() {


			try {
				String filePath = fc.getSelectedFile().toString();
				FileReader fileReader = new FileReader(filePath);
				BufferedReader bufferedReader = new BufferedReader(fileReader);
				String line;
				DateTimeFormatter fileReaderFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
				while ((line = bufferedReader.readLine()) != null) {
					String[] str = line.split(" = ");
					String name = str[1];

					line = bufferedReader.readLine();
					str = line.split(" = ");
					LocalDate birthdate = LocalDate.parse(str[1], fileReaderFormatter);

					line = bufferedReader.readLine();
					str = line.split(" = ");
					double weight = Double.parseDouble(str[1]);

					line = bufferedReader.readLine();
					str = line.split(" = ");
					int height = Integer.parseInt(str[1]);

					line = bufferedReader.readLine();
					str = line.split(" = ");
					Gender gender;
					if (str[1].equals("MALE")) {
						gender = Gender.MALE;
					} else {
						gender = Gender.FEMALE;
					}

					Patient patientToAdd;
					patientToAdd = new Patient(name, birthdate, weight, height, gender);
					profileList.add(patientToAdd);
				}
				bufferedReader.close();
				fileReader.close();
			} catch (IOException ex) {
				System.err.println("Error while reading");
				ex.printStackTrace(System.err);
			}
		}

	}

