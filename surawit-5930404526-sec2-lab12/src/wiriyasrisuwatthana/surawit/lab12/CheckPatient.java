package wiriyasrisuwatthana.surawit.lab12;

import wiriyasrisuwatthana.surawit.lab4.Gender;
import java.time.LocalDate;

public class CheckPatient {
	public static String checkName(String name) throws NoNameException {
		if (name.isEmpty())
			throw new NoNameException();
		return name;
	}

	public static Gender checkGender(boolean male, boolean female) throws NoGenderException {
		if (!(male || female))
			throw new NoGenderException();
		if (male)
			return Gender.MALE;
		else
			return Gender.FEMALE;
	}

	public static String checkAddress(String address) throws NoAddressException {
		if (address.isEmpty())
			throw new NoAddressException();
		return address;
	}

	public static String checkBirthdate(String birthdate) throws NoBirthdateException {
		if (birthdate.isEmpty())
			throw new NoBirthdateException();
		return birthdate;
	}
}
