package wiriyasrisuwatthana.surawit.lab12;

import javax.swing.*;

import wiriyasrisuwatthana.surawit.lab4.Gender;
import wiriyasrisuwatthana.surawit.lab4.Patient;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.format.DateTimeParseException;
import java.util.Objects;

public class PatientFormV14 extends PatientFormV13 {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2365384417402515243L;

	public PatientFormV14(String formName) {
		super(formName);
	}

	@Override
	public void addListeners() {
		super.addListeners();
		okButton.removeActionListener(okButton.getActionListeners()[0]);
		okButton.addActionListener(this);
	}

	public static void createAndShowGUI() {
		PatientFormV14 patientForm14 = new PatientFormV14("Patient Form V14");
		patientForm14.addComponents();
		patientForm14.addComponents();
		patientForm14.pack();
		patientForm14.addListeners();
		patientForm14.setFrameFeatures();

	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				createAndShowGUI();
			}
		});
	}

	@Override
	public void handleOKButton() {
		Patient patientToAdd;

		try {
			String name = CheckPatient.checkName(nameTextField.getText());
			String birthDate = CheckPatient.checkBirthdate(birthDateTextField.getText());
			double weight = Double.parseDouble(weightTextField.getText());
			int height = Integer.parseInt(heightTextField.getText());
			Gender gender = CheckPatient.checkGender(maleButton.isSelected(), femaleButton.isSelected());
			CheckPatient.checkAddress(addressText.getText());

			patientToAdd = new Patient(name, birthDate, weight, height, gender);
			profileList.add(patientToAdd);

			super.handleOKButton();
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(this, "Please enter weight in double and height in int");
		} catch (DateTimeParseException e) {
			JOptionPane.showMessageDialog(this, "Please enter Birthdate in the format DD.MM.YYYY ex. 22.02.2000");
		} catch (NoNameException e) {
			JOptionPane.showMessageDialog(this, "Name has not been entered.");
		} catch (NoGenderException e) {
			JOptionPane.showMessageDialog(this, "No gender has been selected.");
		} catch (NoAddressException e) {
			JOptionPane.showMessageDialog(this, "Address has not been entered.");
		} catch (NoBirthdateException e) {
			JOptionPane.showMessageDialog(this, "Please enter birthdate.");
		}
	}
}
