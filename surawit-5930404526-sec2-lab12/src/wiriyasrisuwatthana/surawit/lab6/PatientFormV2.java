package wiriyasrisuwatthana.surawit.lab6;

import java.awt.BorderLayout;
/*	
 * This is program extends form patientFormV1 but add genderRadioButton & AddressTextArea 
 * 
 *	@author  Surawit Wiriyasrisuwatthana 
 * 	
 * 	@version 1.0
 * 
 */
import java.awt.GridLayout;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

public class PatientFormV2 extends PatientFormV1 {

	public PatientFormV2(String string) {
		super(string);
	}

	private static final long serialVersionUID = 1L;

	// Create Variable
	protected JLabel genderLabel, addressLabel;
	protected JRadioButton maleButton, femaleButton;
	protected JPanel genderButtonPanel, genderPanel, addressPanel, patientV2Panel;
	protected ButtonGroup group;
	protected JTextArea addressText;
	protected JScrollPane scrollerAddress;

	@Override

	// addComponents Method
	protected void addComponents() {

		super.addComponents();

		// Panel
		genderPanel = new JPanel();
		genderButtonPanel = new JPanel();
		addressPanel = new JPanel();
		patientV2Panel = new JPanel();

		// Label
		genderLabel = new JLabel("Gender:");
		addressLabel = new JLabel("Address:");

		// JRadioButton and group it
		maleButton = new JRadioButton("Male");
		femaleButton = new JRadioButton("Female");
		group = new ButtonGroup();

		patientV2Panel.setLayout(new BorderLayout());

		genderButtonPanel.add(genderLabel);
		genderButtonPanel.add(maleButton);
		genderButtonPanel.add(femaleButton);

		group.add(maleButton);
		group.add(femaleButton);

		genderPanel.setLayout(new GridLayout(0, 2));
		genderPanel.add(genderLabel);
		genderPanel.add(genderButtonPanel);

		addressText = new JTextArea(2, 35);
		scrollerAddress = new JScrollPane(addressText);
		addressText.setLineWrap(true);
		addressText.setWrapStyleWord(true);
		addressText.setText("Department of Computer Engineering, Faculty of Engineering, Khon Kaen University,");
		addressText.append("Mittraparp Rd., T. Naimuang, A. Muang, Khon Kaen, Thailand, 40002");
		addressText.setEditable(true);

		addressPanel.setLayout(new GridLayout(0, 1));
		addressPanel.add(addressLabel);
		addressPanel.add(scrollerAddress);

		patientV2Panel.add(inputPanel, BorderLayout.NORTH);
		patientV2Panel.add(genderPanel, BorderLayout.CENTER);
		patientV2Panel.add(addressPanel, BorderLayout.SOUTH);

		mixPanel.add(patientV2Panel, BorderLayout.CENTER);

		add(mixPanel);

	}

	// createAndShowGUI Method
	public static void createAndShowGUI() {
		PatientFormV2 patientForm2 = new PatientFormV2("Patient Form V2");
		patientForm2.addComponents();
		patientForm2.setFrameFeatures();

	}
	
	// main method
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
