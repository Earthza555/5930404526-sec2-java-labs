package wiriyasrisuwatthana.surawit.lab6;

import java.awt.*;
import javax.swing.*;

/**
 * This program is about create button's name OK & Cancel
 * 
 * @author Surawit Wiriyasrisuwatthana
 * 
 * @version 1.0
 * 
 */

public class MySimpleWindow extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5536939968914488142L;
	protected JButton okButton;
	protected JButton cancelButton;
	protected JPanel mswPanel;

	public MySimpleWindow(String string) {
		super(string);
	}

	protected void addComponents() {
		okButton = new JButton("OK");
		cancelButton = new JButton("Cancel");
		mswPanel = new JPanel();
		
		// add button to JPanel
		mswPanel.add(cancelButton);
		mswPanel.add(okButton);

		// add JPanel in JFrame
		add(mswPanel);
	}

	// setFrameFeatures Method
	protected void setFrameFeatures() {
		pack();
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		int w = getSize().width;
		int h = getSize().height;
		int x = (dim.width - w) / 2;
		int y = (dim.height - h) / 2;
		setLocation(x, y);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}

	// createAndShowGUI Method
	public static void createAndShowGUI() {
		MySimpleWindow msw = new MySimpleWindow("My Simple Window");
		msw.addComponents();
		msw.setFrameFeatures();
	}

	// main method
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();

			}
		});

	}
}
