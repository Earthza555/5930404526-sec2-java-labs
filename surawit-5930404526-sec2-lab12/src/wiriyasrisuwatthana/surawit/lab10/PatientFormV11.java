package wiriyasrisuwatthana.surawit.lab10;

/**
 * This program is develop from PatientV10 and add sortMI for sorting patient by weight
 * 
 * @author Surawit Wiriyasrisuwatthana
 * 
 * @version 1.0
 * 
 */
import java.awt.event.ActionEvent;
import java.util.Collections;
import java.util.Comparator;
import javax.swing.SwingUtilities;
import wiriyasrisuwatthana.surawit.lab4.Patient;

public class PatientFormV11 extends PatientFormV10 implements Comparable<PatientFormV10> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9109234080414140611L;

	// constructor
	public PatientFormV11(String string) {
		super(string);

	}

	// main method
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	// addComponents Method by super class
	public void addComponents() {
		pack();
		super.addComponents();
	}

	// addListeners Method from superclass and add sortMenuItem
	protected void addListeners() {
		super.addListeners();
		sortMI.addActionListener(this);
	}

	// createAndShowGUI Method
	public static void createAndShowGUI() {
		PatientFormV11 patientForm11 = new PatientFormV11("Patient Form V11");
		patientForm11.addComponents();
		patientForm11.setFrameFeatures();
		patientForm11.addListeners();
	}

	// actionPerformed Method
	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
		Object src = event.getSource();

		if (src == sortMI) {
			sortPatient();
			displayPatient();
		}
	}

	// Comparator for compare weight in patient and sorting
	public static Comparator<Patient> weightCompare = new Comparator<Patient>() {
		public int compare(Patient patient1, Patient patient2) {
			return (int) (patient1.getWeight() - patient2.getWeight());
		}
	};

	public void sortPatient() {
		Collections.sort(profileList, PatientFormV11.weightCompare);
	}

	// implement method
	@Override
	public int compareTo(PatientFormV10 o) {

		return 0;
	}
}
