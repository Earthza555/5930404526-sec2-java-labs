package wiriyasrisuwatthana.surawit.lab10;

/**
 * This program is like PatientFormV9 but use arraylist for add patient nolimit
 * 
 * @author Surawit Wiriyasrisuwatthana
 * 
 * @version 1.0
 * 
 */
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import wiriyasrisuwatthana.surawit.lab4.*;
import wiriyasrisuwatthana.surawit.lab9.*;

public class PatientFormV10 extends PatientFormV9 implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3341398360752100293L;
	
	// declare variable
	JMenu dataMenu;
	JMenuItem displayMI, sortMI, searchMI, removeMI;
	protected ArrayList<Patient> profileList = new ArrayList<Patient>();;
	protected Gender gender;
	protected String profile = "";
	protected Patient newProfile;
	protected double weight;
	protected int height;

	public PatientFormV10(String string) {
		super(string);
	}

	// addcomponent from patientformv8
	public void addComponents() {
		pack();
		super.addComponents();
		dataMenu = new JMenu("Data");
		displayMI = new JMenuItem("Display");
		sortMI = new JMenuItem("Sort");
		searchMI = new JMenuItem("Search");
		removeMI = new JMenuItem("Remove");

		// add menuItem to Menu
		dataMenu.add(displayMI);
		dataMenu.add(sortMI);
		dataMenu.add(searchMI);
		dataMenu.add(removeMI);
		menuBar.add(dataMenu);
	}

	// addListeners method from super class and add displayMI
	protected void addListeners() {
		super.addListeners();
		displayMI.addActionListener(this);
	}

	// createAndShowGUI Method
	public static void createAndShowGUI() {
		PatientFormV10 patientForm10 = new PatientFormV10("Patient Form V10");
		patientForm10.addComponents();
		patientForm10.setFrameFeatures();
		patientForm10.addListeners();
	}

	// main method
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});

	}

	// addPateint Method use when click okButton
	public void addPatient() {
		if (maleButton.isSelected()) {
			gender = Gender.MALE;
		} else if (femaleButton.isSelected()) {
			gender = Gender.FEMALE;
		}
		weight = Double.parseDouble(weightTextField.getText());
		height = Integer.parseInt(heightTextField.getText());
		newProfile = new Patient(nameTextField.getText(), birthDateTextField.getText(), gender, weight, height);
		profileList.add(newProfile);

	}

	// displayPatient Method use when click displayMenuItem
	public void displayPatient() {

		int numProfile = profileList.size();
		for (int i = 0; i < numProfile; i++) {
			profile += (i + 1) + ": " + profileList.get(i).toString() + "\n";

		}
		JOptionPane.showMessageDialog(this, profile);
	}

	// actionPerformed Method
	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
		Object src = event.getSource();
		if (src == okButton) {
			addPatient();
		} else if (src == displayMI) {
			displayPatient();
		}
	}
}
