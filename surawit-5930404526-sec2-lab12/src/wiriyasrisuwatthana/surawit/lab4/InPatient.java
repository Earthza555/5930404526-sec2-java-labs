/* InPatient using admitDate and dischargeDate variables
 *  created by Surawit W.
 *
 */
package wiriyasrisuwatthana.surawit.lab4;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

public class InPatient extends Patient {
	private LocalDate admitDate;
	private LocalDate dischargeDate;

	DateTimeFormatter germanFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM).withLocale(Locale.GERMAN);

	public InPatient(String name, String birthdateStr, Gender gender, double weight, int height, String admitDateStr,
			String dischargeDateStr) {
		super(name, birthdateStr, gender, weight, height);
		this.admitDate = LocalDate.parse(admitDateStr, germanFormatter);
		;
		this.dischargeDate = LocalDate.parse(dischargeDateStr, germanFormatter);
		;
	}

	@Override
	public String toString() {
		return "InPatient [" + getName() + ", " + getBirthdate() + ", " + getGender() + ", " + getWeight() + " kg. "
				+ ", " + getHeight() + " cm. " + ", admitDate=" + admitDate + ", dischargeDate=" + dischargeDate + "]";
	}

}
