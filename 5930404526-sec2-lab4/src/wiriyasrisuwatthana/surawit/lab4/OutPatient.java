/*OutPatient using calculate the number of days between created by Surawit W.
 * 
 */
package wiriyasrisuwatthana.surawit.lab4;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.ChronoUnit;
import java.util.Locale;

public class OutPatient extends Patient {
	private LocalDate visitDate;
	public static String hospitalName = "Srinakarin";
	DateTimeFormatter germanFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM).withLocale(Locale.GERMAN);

	// constructor 1
	public OutPatient(String name, String birthdayStr, Gender gender, double weight, int height) {
		super(name, birthdayStr, gender, weight, height);

	}

	// constructor 2 with visitDate
	public OutPatient(String name, String birthdayStr, Gender gender, double weight, int height, String visitDateStr) {
		super(name, birthdayStr, gender, weight, height);
		this.visitDate = LocalDate.parse(visitDateStr, germanFormatter);
	}

	// methods to set and get each information
	public LocalDate getVisitDate() {
		return visitDate;
	}

	public void setVisitDate(String visitDate) {
		this.visitDate = LocalDate.parse(visitDate, germanFormatter);
	}

	// calculate the number of days between
	public void displayDaysBetween(OutPatient people) {
		long daysBetween = getDaysCountBetweenDates(people.getVisitDate(), visitDate);
		System.out.println("Chujai visited after " + people.getName() + " for " + daysBetween + " days.");
	}

	public long getDaysCountBetweenDates(LocalDate dateBefore, LocalDate dateAfter) {
		return ChronoUnit.DAYS.between(dateBefore, dateAfter);
	}

	@Override
	public String toString() {
		return "OutPatient [" + getName() + ", birthdate = " + getBirthdate() + ", " + getGender() + ", " + getWeight()
				+ " kg." + ", " + getHeight() + " cm." + " visitDate = " + visitDate + "]";
	}

}
