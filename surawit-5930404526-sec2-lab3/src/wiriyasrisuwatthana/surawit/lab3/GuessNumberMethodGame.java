/*	The program defines at least two subroutines, namely genAnswer(), and playGame()
  	created by  Surawit W.
  	593040452-6
  	
 */
package wiriyasrisuwatthana.surawit.lab3;

import java.util.Scanner;

public class GuessNumberMethodGame {

	static int remainingGuess = 7; // number of possible guess
	static int answer;
	static final int MIN = 0 ,MAX = 100 ;  
	
	public static void main(String[] args) {
		genAnswer();
		playGame();
		
	}
	
	public static void genAnswer(){
		
		 answer =  MIN + (int)(Math.random() * ((MAX - MIN) + 1)) ; // random number for answer.
		
	}
	public static void playGame(){
				Scanner guessnumber = new Scanner(System.in);

		for (; remainingGuess > 0; remainingGuess--) {  // loop for guess number.

			System.out.println("Number of remaining guess is " + remainingGuess);

			System.out.print("Enter a guess: ");
			int num = guessnumber.nextInt();
			if (num < answer) {
				System.out.println("Higher!");
			} else if (num > answer) {
				System.out.println("Lower!");
			} else if (num == answer) {
				System.out.println("Correct!");
				System.exit(0);
			}
		}
		System.out.println("You ran out of uesses. The number was " + answer);
		guessnumber.close();
		
	}

}
