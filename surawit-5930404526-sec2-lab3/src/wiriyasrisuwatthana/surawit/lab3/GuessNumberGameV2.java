/*	GuessNumberGameV2 is an improved version of the previous program GuessNumberGame
  	created by  Surawit W.
  	593040452-6
  	
 */
package wiriyasrisuwatthana.surawit.lab3;

import java.util.Scanner;

public class GuessNumberGameV2 {

	static int minmaxrandom; // method for specify min, max and random number for answer.				 
	static int playGame; // method for playgame like a GuessNumberGame .
	static int answer, min, max; // specify variable for global in GuessNumberGameV2.
									 

	public static void main(String[] args) {
		minmaxrandom();
		playGame();
		playagain();

	}

	public static void minmaxrandom() {
		Scanner guessMinMax = new Scanner(System.in);
		System.out.print("Enter min and max of random numbers: ");
		min = guessMinMax.nextInt();
		max = guessMinMax.nextInt();
		answer = min + (int) (Math.random() * ((max - min) + 1));
		if (max < min) {
			int change = min;
			min = max;
			max = change;

		}

	}

	public static void playGame() {
		Scanner possibleGuess = new Scanner(System.in);
		System.out.print("Enter the number possible guess : ");
		int possiblenum = possibleGuess.nextInt();

		for (; possiblenum > 0; possiblenum--) {

			System.out.println("Number of remaining guess is " + possiblenum);

			System.out.print("Enter a guess: ");
			int num = possibleGuess.nextInt();
			if (num > max || num < min) {
				System.out.println("Number must be in " + min + " and " + max);
				possiblenum++;
			} else if (num < answer) {
				System.out.println("Higher!");
			} else if (num > answer) {
				System.out.println("Lower!");
			} else if (num == answer) {
				System.out.println("Correct!");
				System.exit(0);

			}
		}
		System.out.println("You ran out of uesses. The number was " + answer);

	}

	public static void playagain() {
		Scanner again = new Scanner(System.in);
		System.out.println("Want to play again? (\"Y\" or \"y\")"); // for play  game again.
		String playagain2 = again.next();
		if (playagain2.equalsIgnoreCase("y")) {
			minmaxrandom();
			playGame();
		}

	}
}
