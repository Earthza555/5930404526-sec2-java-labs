/* 	this program is about simulates a guessing game of 
	which a user guesses number between 0 to 100.
    created by  Surawit W.
  	593040452-6
  	Thank you for playing! <3
 */
package wiriyasrisuwatthana.surawit.lab3;

import java.util.Scanner;

public class GuessNumberGame { 

	public static void main(String[] args) {

		int randomNum = 0 + (int) (Math.random() * ((100 - 0) + 1));   //  random nunber
		Scanner guessnumber = new Scanner(System.in);   

		for (int i = 7; i > 0; i--) {    // possible guess

			System.out.println("Number of remaining guess is " + i); 

			System.out.print("Enter a guess: ");
			int num = guessnumber.nextInt();  
			if (num < randomNum) {
				System.out.println("Higher!");
			} else if (num > randomNum) {
				System.out.println("Lower!");
			} else if (num == randomNum) {
				System.out.println("Correct!");
				System.exit(0);
			}
		}
		System.out.println("You ran out of uesses. The number was " + randomNum); //Show when you ran out of possible guess.
		guessnumber.close();
	}

}
