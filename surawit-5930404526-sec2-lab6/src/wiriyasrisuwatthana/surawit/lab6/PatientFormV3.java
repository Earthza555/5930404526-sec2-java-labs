package wiriyasrisuwatthana.surawit.lab6;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
/*	
 *  This program is extends form PatientFormV2 and addMenu add combobox
 * 
 *	@author  Surawit Wiriyasrisuwatthana 
 * 	
 * 	@version 1.0
 * 
 */
public class PatientFormV3 extends PatientFormV2{

	private static final long serialVersionUID = -1685881197765118468L;
	public PatientFormV3(String string) {
		super(string);}
	
	// create variable
	protected JMenuBar menuBar;
	protected JMenu fileMenu,configMenu;
	protected JMenuItem newItem, openItem, saveItem, exitItem, colorItem, sizeItem;
	protected JPanel patientv3Panel,typePanel;
	protected JLabel typeLabel;
	protected JComboBox typeComboBox ;
	protected String[] patientTypeString = {"Inpatient","Outpatient"};
	
	
	@Override
		protected void addComponents(){
			super.addComponents();
		
		// declare variable
		patientv3Panel = new JPanel();
		typePanel = new JPanel();
		typeLabel = new JLabel("Type:");
		// menubar setup
		menuBar = new JMenuBar();
		
		typeComboBox = new JComboBox(patientTypeString);
		typeComboBox.setSelectedIndex(1);
		
		// menuitem setup
		fileMenu = new JMenu("File");
		configMenu = new JMenu("Config");
		newItem = new JMenuItem("New");
		openItem = new JMenuItem("Open");
		saveItem = new JMenuItem("Save");
		exitItem  = new JMenuItem("Exit");
		colorItem = new JMenuItem("Color");
		sizeItem = new JMenuItem("Size");
		
		//addItem to fileMenu
		fileMenu.add(newItem);
		fileMenu.add(openItem);
		fileMenu.add(saveItem);
		fileMenu.add(exitItem);
		
		//addItem to configMenu
		configMenu.add(colorItem);
		configMenu.add(sizeItem);
		
		// add menu to menuBar
		menuBar.add(fileMenu);
		menuBar.add(configMenu);
		
		//add typeLabel & typeComboBox to typePanel
		typePanel.setLayout(new GridLayout(0, 2));
		typePanel.add(typeLabel);
		typePanel.add(typeComboBox);
		
		//add everything to patientv3Panel and finally add to mixPanel
		patientv3Panel.setLayout(new BorderLayout());
		patientv3Panel.add(menuBar,BorderLayout.NORTH);
		patientv3Panel.add(patientV2Panel,BorderLayout.CENTER);
		patientv3Panel.add(typePanel,BorderLayout.SOUTH);
		
		mixPanel.add(patientv3Panel);
		
	}
	
	// createAndShowGUI Method
	public static void createAndShowGUI(){
		PatientFormV3 patientForm3 = new PatientFormV3("Patient Form V3");
		patientForm3.addComponents();
		patientForm3.setFrameFeatures();

}
	
	// main Method
	public static void main(String[] args) {
	SwingUtilities.invokeLater(new Runnable() {
		public void run() {
			createAndShowGUI();
		}
	});
}
}
