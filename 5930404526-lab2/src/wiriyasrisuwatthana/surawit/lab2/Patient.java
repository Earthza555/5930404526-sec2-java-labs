package wiriyasrisuwatthana.surawit.lab2;

public class Patient {

	public static void main(String[] args) {
		if (args.length != 4) {
			System.err.println("Patient <name> <gender> <weight> <height>");
			System.exit(0);
		}

		String patientname = args[0];
		String Female = "female";
		String Male = "male";
		String gender = args[1].toLowerCase();
		double weight = Double.parseDouble(args[2]);
		int height = Integer.parseInt(args[3]);
		System.out.println("This patient name is " + patientname);
		if (gender.equals(Female)) {
			System.out.println("Her weight is " + weight + "kg. and height is" + height + " cm.");
		} else if (gender.equals(Male)) {
			System.out.println("His weight is " + weight + " kg. and height is " + height + " cm.");
		} else {
			System.out.println("Please enter gender as only Male or Female");
		}

	}

}
