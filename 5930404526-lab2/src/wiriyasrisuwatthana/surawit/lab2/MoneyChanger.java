package wiriyasrisuwatthana.surawit.lab2;

public class MoneyChanger {

	public static void main(String[] args) {
		int money = Integer.parseInt(args[0]);
		int exchange = 1000 - money;

		System.out.println("You give 1000 for the cost as " + money + " Baht.");
		System.out.println("You will receive exchange as " + exchange + " Baht.");

		if (exchange > 0) {

			int note500 = exchange / 500;
			int note100 = (exchange % 500) / 100;
			int note50 = ((exchange % 500) % 100) / 50;
			int note20 = (((exchange % 500) % 100) % 50) / 20;
			if (note500 > 0) {
				System.out.print(note500 + " of 500 bank note; ");
			}
			if (note100 > 0) {
				System.out.print(note100 + " of 100 bank note; ");
			}
			if (note50 > 0) {
				System.out.print(note500 + " of 50 bank note; ");
			}
			if (note20 > 0) {
				System.out.print(note20 + " of 20 bank note; ");
			}
		} else {

			System.out.println("No Change");

		}

	}

}
