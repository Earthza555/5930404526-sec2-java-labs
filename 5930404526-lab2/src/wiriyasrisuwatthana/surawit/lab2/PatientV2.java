package wiriyasrisuwatthana.surawit.lab2;

public class PatientV2 {

	public static void main(String[] args) {

		String name = args[0];
		double weight = Double.parseDouble(args[2]);
		int height = Integer.parseInt(args[3]);
		String gender = args[1];
		if (args.length == 4) {

			System.out.println("This patient name is " + name);
			if (weight < 0) {
				System.out.println("Weight must be non-negative");
				System.exit(0);
			} else if (height < 0) {
				System.out.println("Height must be non-negative");
				System.exit(0);
			} else if (weight < 0 && height < 0) {
				System.out.println("Weight must be non-negative");
				System.exit(0);
			}

			if (gender.equalsIgnoreCase("Female")) {
				System.out.println("Her weight is " + weight + " kg. and height is " + height + " cm.");
			}

			else if (gender.equalsIgnoreCase("Male")) {
				System.out.println("His weight is " + weight + " kg. and height is " + height + " cm.");
			}

			else {
				System.err.println("Please enter gender as only Male or Female");
			}
		}

		else
			System.err.println("Patient <name> <gender> <weight> <height>");

	}

}
