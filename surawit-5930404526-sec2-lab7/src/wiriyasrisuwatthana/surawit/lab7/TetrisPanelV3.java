package wiriyasrisuwatthana.surawit.lab7;

/**
 * TetrisV3
 * 
 * This class use in TetrisV3
 * 
 * 
 * @author Surawit Wiriyasrisuwatthana
 * 
 * @version 1.0
 *
 *         16-03-2017
 *
 */
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

public class TetrisPanelV3 extends TetrisPanelV2 {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4702629280473665644L;
	
	// create variable
	protected final static int RECT_WIDTH = 60;
	protected final static int RECT_HEIGHT = 40;
	protected final static int SPEED = 20;
	int x, y;
	Thread running;
	private ArrayList<Rectangle2D.Double> notMovingRect;
	private ArrayList<Color> notMovingRectColor;
	private int numNotMovingRect = 0;
	private Color randomColor = randomColor();
	Rectangle2D.Double rectangleFall;

	public TetrisPanelV3() {
		super();
		setBackground(Color.WHITE);
		notMovingRect = new ArrayList<Rectangle2D.Double>();
		notMovingRectColor = new ArrayList<Color>();
	}

	@Override
	public void paint(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;	// use graphic for fill and draw
		rectangleFall = new Rectangle2D.Double(x, y, RECT_WIDTH, RECT_HEIGHT);
		for (int i = 0; i < numNotMovingRect; i++) {  	// loop for create rectangle
			g2d.setColor(notMovingRectColor.get(i));
			g2d.fill(notMovingRect.get(i));
			g2d.setColor(Color.DARK_GRAY);
			g2d.draw(notMovingRect.get(i));
		}

		g2d.setColor(randomColor);
		g2d.fill(rectangleFall);
		g2d.setColor(Color.DARK_GRAY);
		g2d.draw(rectangleFall);

	}

	@Override
	public void run() {
		while (true) {
			if (y >= HEIGHT - RECT_HEIGHT - SPEED + 15) {

				notMovingRectColor.add(randomColor);	
				notMovingRect.add(rectangleFall);
				randomColor = randomColor();
				numNotMovingRect++;
				x = 0 + (int) (Math.random() * ((600 - 0) + 1));
				y = 0;
			}
			repaint();
			y += SPEED;

			try {
				Thread.sleep(25);
			} catch (InterruptedException ex) {

			}
		}

	}

	// randomColor Method
	public Color randomColor() {
		int red = 0 + (int) (Math.random() * ((255 - 0) + 1));
		int green = 0 + (int) (Math.random() * ((255 - 0) + 1));
		int blue = 0 + (int) (Math.random() * ((255 - 0) + 1));
		return new Color(red, green, blue);
	}
}
