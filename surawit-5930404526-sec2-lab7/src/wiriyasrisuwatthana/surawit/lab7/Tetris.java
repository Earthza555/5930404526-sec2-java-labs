package wiriyasrisuwatthana.surawit.lab7;
/**
 * Tetris Game
 * 
 * This program is to create tetrimino 7 pieces and word "Tetris"
 * 
 * 
 * @author Surawit Wiriyasrisuwatthana
 * 
 * @version 1.0
 *
 *         16-03-2017
 *
 */


import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class Tetris extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4826491729865488023L;
		
		TetrisPanel tetris;
		public Tetris(String title) {
		super(title);
	}

	// setFrameFeatures Method
	protected void setFrameFeatures() {
		pack();
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		int w = getSize().width;
		int h = getSize().height;
		int x = (dim.width - w) / 2;
		int y = (dim.height - h) / 2;
		setLocation(x, y);
		setVisible(true);
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	// createAndShowGUI Method
	public static void createAndShowGUI() {
		Tetris tetris = new Tetris("CoE Tetris Game");		// The title of the program 
		tetris.addComponents();
		tetris.setFrameFeatures();
	}
	
	// Main 
	public static void main(String[] args) {

		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
	
	// addComponents method
	public void addComponents() {
		tetris = new TetrisPanel();
		add(tetris);
	}

}
