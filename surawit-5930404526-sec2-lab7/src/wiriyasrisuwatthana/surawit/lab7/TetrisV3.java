package wiriyasrisuwatthana.surawit.lab7;

/**
 * TetrisV3
 * 
 * This class is about rectangle random falling and not disappear
 * 
 * 
 * @author Surawit Wiriyasrisuwatthana
 * 
 * @version 1.0
 *
 * 16-03-2017
 *
 */
import javax.swing.SwingUtilities;

public class TetrisV3 extends TetrisV2 {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5383659258621313267L;

	public TetrisV3(String title) {
		super(title);
	}

	TetrisPanelV3 tetrisPanel3;
	
	// main
	public static void main(String[] args) {

		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	// addComponents Method
	public void addComponents() {
		tetrisPanel3 = new TetrisPanelV3();
		add(tetrisPanel3);
	}
	
	// createAndShowGUI Method
	public static void createAndShowGUI() {
		TetrisV3 tetris3 = new TetrisV3("Rectangle Dropping V2");
		tetris3.addComponents();
		tetris3.setFrameFeatures();
	}
}
