package wiriyasrisuwatthana.surawit.lab7;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;

public class TetrisPanel extends JPanel {
	protected final static int WIDTH = 600;
	protected final static int HEIGHT= 400;
	protected final static int BOARD_WIDTH = 30;
	protected final static int BOARD_HEIGHT = 20;
	protected final static int SQUARE_WIDTH = WIDTH/BOARD_WIDTH;
	protected final static int SQUARE_HEIGHT = HEIGHT/BOARD_HEIGHT;	
	
	
	@Override
	public Dimension getPreferredSize(){
		return new Dimension(WIDTH, HEIGHT);
	}
	
	public TetrisPanel(){
		super();
		setBackground(Color.LIGHT_GRAY);
	}
	
	@Override
	public void paint(Graphics drawing){
		super.paintComponent(drawing);
		setPreferredSize( getPreferredSize());
		Graphics2D draw= (Graphics2D) drawing;
		
		draw.setPaint(Color.RED);
		draw.setFont(new Font("Serif",Font.BOLD,30));
		String tetris = "Tetris";
		int widghtfont = draw.getFontMetrics().stringWidth(tetris);
		draw.drawString(tetris, (WIDTH - widghtfont)/2, HEIGHT/4 );
		
		TetrisShape zshape = new TetrisShape(Tetrimino.Z_SHAPE);
		TetrisShape sshape = new TetrisShape(Tetrimino.S_SHAPE);
		TetrisShape ishape = new TetrisShape(Tetrimino.I_SHAPE);
		TetrisShape tshape = new TetrisShape(Tetrimino.T_SHAPE);
		TetrisShape oshape = new TetrisShape(Tetrimino.O_SHAPE);
		TetrisShape lshape = new TetrisShape(Tetrimino.L_SHAPE);
		TetrisShape jshape = new TetrisShape(Tetrimino.J_SHAPE);
		TetrisShape shape[] = {zshape,sshape,ishape,tshape,oshape,lshape,jshape};
		int startPosX = 2*SQUARE_WIDTH;
		int startPosY = 8*SQUARE_HEIGHT;
		
	for(int j = 0 ; j<7 ; j++){
		
		
		for(int i = 0 ;i < 4;i++){
			int blockPosX = startPosX +(shape[j].coordinateX[i] * SQUARE_WIDTH);
			int blockPosY = startPosY +(shape[j].coordinateY[i] * SQUARE_HEIGHT);
			draw.setColor(shape[j].getColor());
			draw.fillRect(blockPosX, blockPosY, SQUARE_WIDTH, SQUARE_HEIGHT);
			draw.setColor(Color.BLACK);
			draw.drawRect(blockPosX, blockPosY, SQUARE_WIDTH, SQUARE_HEIGHT);
			
			
	}startPosX = startPosX +4*SQUARE_WIDTH;
	}	
		
		
		
	}
	
	
}
